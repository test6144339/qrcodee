import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import QRCodeGenerator from '../components/QRCodeGenerator';
import { BarCodeScanner } from 'expo-barcode-scanner';

const AlumnoScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>EDUQR</Text>
     
      <QRCodeGenerator />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    textAlign: 'center',
    fontSize: 45,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 40,
    color: '#003E80',
  },
});

export default AlumnoScreen;
