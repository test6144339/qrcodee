import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';

const QRCodeReader = ({ onBarCodeScanned }) => {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [scannedData, setScannedData] = useState(null);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = ({ data }) => {
    setScanned(true);
    setScannedData(data);

    // Reinicia el escaneo después de 2 segundos
    setTimeout(() => {
      setScanned(false);
      setScannedData(null);
    }, 2000);

    // Pasa los datos escaneados a la función proporcionada
    onBarCodeScanned && onBarCodeScanned({ data });
  };

  const generateRandomName = () => {
    const firstNames = ['Carlos', 'Ana', 'Miguel', 'María', 'José', 'Laura', 'David', 'Sofía'];
    const lastNames = ['González', 'Rodríguez', 'Fernández', 'López', 'Martínez', 'Pérez'];

    const randomFirstName = firstNames[Math.floor(Math.random() * firstNames.length)];
    const randomLastName = lastNames[Math.floor(Math.random() * lastNames.length)];

    return `${randomFirstName} ${randomLastName}`;
  };

  const generateRandomGroup = () => {
    return 'Grupo ' + String.fromCharCode(65 + Math.floor(Math.random() * 26));
  };

  return (
    <View style={styles.container}>
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />

      {scannedData && (
        <View style={styles.overlay}>
          <Text style={styles.overlayText}>
            La matrícula del alumno es: {scannedData}
            {"\n"}
            Nombre: {generateRandomName()}
            {"\n"}
            Grupo: {generateRandomGroup()}
          </Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  overlayText: {
    color: 'white',
    fontSize: 16,
  },
});

export default QRCodeReader;
