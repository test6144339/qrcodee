import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, SafeAreaView, Image, TouchableOpacity, ScrollView } from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import { Formik } from "formik";

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState('');

 
  const handleLogin = () => {
    console.log('Correo ingresado:', email);
  
    const isMaestro = /^[a-zA-Z]+@/.test(email);
    const isAlumno = /^\d+@/.test(email);
  
    console.log('isMaestro:', isMaestro);
    console.log('isAlumno:', isAlumno);
  
    if (isMaestro) {
      console.log('Navegar a Maestro');
      navigation.navigate('Maestro', {
        userData: { name: 'Nombre del Maestro', /* otros datos del maestro */ }
      });
    } else if (isAlumno) {
      console.log('Navegar a Alumno');
      navigation.navigate('Alumno', {
        userData: { name: 'Nombre del Alumno', /* otros datos del alumno */ }
      });
    } else {
      console.log('Correo no válido');
      // Puedes realizar alguna acción adicional, como mostrar un mensaje de error al usuario.
    }
  };
  
  
  
  return (
    <ScrollView style={styles.containerScroll}>
    <KeyboardAvoidingView behavior="position" style={styles.mainContainer}>
        <View style={styles.containerSvg}>
            <Image
                source={require('../assets/eduqr.png')}
                style={{ width: 350, height: 350, top: 60}}
            />
            <Text style={styles.title}>EDUQR</Text>  
            <View style={styles.formContainer}>
                        <View style={styles.inputArea}>
                            <TextInput
							style={styles.input}
                                placeholder="Ingrese el correo"
                                value={email}
                                onChangeText={(text) => setEmail(text)}  
                            />
                        </View>
						<TouchableOpacity onPress={handleLogin} style={styles.button}>
        					<Text style={styles.buttonText}>Iniciar Sesión</Text>
      					</TouchableOpacity>                   
                    </View>  
        		</View>		
    </KeyboardAvoidingView>
</ScrollView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
	containerScroll: {
		flex: 1,
		flexGrow: 2,
	},
	mainContainer: {
		backgroundColor: '#FFF',
		height: '150%',
	},
	containerSvg: {
		marginTop: 0,
		alignItems: 'center',
		justifyContent: 'center',
	},
	text3: {
		fontSize: 15,
		color: '#848484',
		marginVertical: 20,
	},
	boldText: {
		fontWeight: 'bold',
		color: '#003E80',
	},
	title:{
        textAlign: 'center',
        fontSize:45,
        fontWeight:'bold',
        marginTop: 100,
        marginBottom: 20,
        marginTop:40,
        color:'#003E80'
    },
	button: {
		padding: 20,
		backgroundColor: '#9807C2',
		borderRadius: 30,
		marginTop: 20,
		alignItems: 'center',
	  },
	buttonText: {
		color: '#fff',
		fontSize: 20,
		fontWeight: 'bold',
	},
	formContainer: {
		alignItems:"center",
		justifyContent:"center",
	},
	inputArea: {
		flexDirection: "row",
		alignItems: "center",
		width: 300,
		backgroundColor: "#ECECEC",
		borderRadius: 30,
		margin: 10,
		height: 60,
		...Platform.select({
			ios: {
				shadowColor: "#000",
				shadowOffset: { width: 0, height: 2 },
				shadowOpacity: 0.2,
				shadowRadius: 2,
			},
			android: {
			  	elevation: 5,
			},
		}),
	},
	input:{
		width: "85%",
		height: 60,	
		fontSize: 15,
		padding: 15,
    },
	error: {
		color: 'red',
		fontSize: 14,
		margin: 1,
		textAlign: 'center',
		fontWeight: "bold",
	},
	textProgress: {
		marginTop: 5,
		fontSize: 20,
		fontWeight: "bold",
		color: "#05668D",
		textAlign: "center",
	},
	modal: {
		alignContent: "center",
		alignSelf: "center",
		alignItems: "center",
		flex: 1,
		width: "100%",
	},
	textProgress: {
		fontSize: 20,
		fontWeight: "bold",
		color: "#05668D",
		textAlign: "center",
	},
	aceptarbtn: {
		backgroundColor: "#05668D",
		padding: 15,
		width: "80%",
		borderRadius: 20,
		marginTop: 20,
	},
	textaceptar: {
		fontSize: 20,
		fontWeight: "bold",
		color: "white",
		textAlign: "center",
	},
	modalResponse: {
		textAlign: "center",
		backgroundColor: "white",
		alignItems: "center",
		padding: 20,
		borderRadius: 20,
	},
});




