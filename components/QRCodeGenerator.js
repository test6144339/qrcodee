import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import { useNavigation } from '@react-navigation/native';
import QRCodeReader from './QRCodeReader';

const QRCodeGenerator = () => {
  const navigation = useNavigation();
  const [userData, setUserData] = useState({
    matricula: '',
    nombre: '',
    grupo: '',
    qrCode: '',
  });

  const generateRandomUserData = () => {
    const newMatricula = Math.floor(Math.random() * 1000000).toString();
    const newNombre = generateRandomName();
    const newGrupo = 'Grupo ' + String.fromCharCode(65 + Math.floor(Math.random() * 26));

    // Utiliza la matrícula como valor del código QR
    setUserData({ matricula: newMatricula, nombre: newNombre, grupo: newGrupo, qrCode: newMatricula });
  };

  const generateRandomName = () => {
    const firstNames = ['Carlos', 'Ana', 'Miguel', 'María', 'José', 'Laura', 'David', 'Sofía'];
    const lastNames = ['González', 'Rodríguez', 'Fernández', 'López', 'Martínez', 'Pérez'];

    const randomFirstName = firstNames[Math.floor(Math.random() * firstNames.length)];
    const randomLastName = lastNames[Math.floor(Math.random() * lastNames.length)];

    return `${randomFirstName} ${randomLastName}`;
  };

  const generateNewQRCode = () => {
    // Utiliza la matrícula actual como valor del nuevo código QR
    setUserData((prevUserData) => ({ ...prevUserData, qrCode: prevUserData.matricula }));
  };

  const logout = () => {
    generateRandomUserData();
    navigation.reset({
      index: 0,
      routes: [{ name: 'Login' }],
    });
  };

  useEffect(() => {
    generateRandomUserData();
  }, []);

  return (
    <View style={styles.container}>
      {/* Muestra el código QR generado */}
      <QRCode value={JSON.stringify(userData.qrCode)} size={300} />

      {/* Muestra información del usuario */}
      <View>
        <Text >Matrícula: {userData.matricula }</Text>
        {/* <Text>Nombre: {userData.nombre}</Text>
        <Text>Grupo: {userData.grupo}</Text> */}
      </View>

      {/* Agrega el componente QRCodeReader */}
      <QRCodeReader onBarCodeScanned={() => {}} />

      {/* Botones para generar un nuevo código QR y cerrar sesión */}
      {/* <TouchableOpacity onPress={generateNewQRCode} style={styles.button}>
        <Text style={styles.buttonText}>Generar Nuevo Código QR</Text>
      </TouchableOpacity> */}
      <TouchableOpacity onPress={logout} style={styles.logoutButton}>
        <Text style={styles.buttonText}>Cerrar Sesión</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  button: {
    padding: 10,
    backgroundColor: '#003E80',
    borderRadius: 5,
    marginTop: 20,
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
  },
  logoutButton: {
    padding: 10,
    backgroundColor: '#9807C2',
    borderRadius: 5,
    marginTop: 20,
    alignItems: 'center',
  },
});

export default QRCodeGenerator;
