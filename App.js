import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './screens/LoginScreen';
import AlumnoScreen from './screens/AlumnoScreen';
import MaestroScreen from './screens/MaestroScreen';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Alumno" component={AlumnoScreen} />
        <Stack.Screen name="Maestro" component={MaestroScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
